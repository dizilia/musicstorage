//
//  APIClient.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation

import Foundation
import Alamofire
import RxSwift

protocol ClientProtocol {

    func request<Response>(_ endpoint: Endpoint<Response>) -> Single<Response>
}

final class APIClient: ClientProtocol {

    private let manager: Alamofire.SessionManager

    private let baseURL = URL(string: "https://staging.lymolymo.com/")!
    private let queue   = DispatchQueue(label: "LimoNetwork")

    init() {

        //        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        //        defaultHeaders["Authorization"] = "Bearer \(accessToken)"

        let configuration = URLSessionConfiguration.default

        // Add `Auth` header to the default HTTP headers set by `Alamofire`
        //        configuration.httpAdditionalHeaders = defaultHeaders

        self.manager = Alamofire.SessionManager(configuration: configuration)
        self.manager.retrier = OAuth2Retrier()
    }

    func request<Response>(_ endpoint: Endpoint<Response>) -> Single<Response> {

        return Single<Response>.create { observer in

            let request = self.manager.request( self.url(path: endpoint.path),
                                                method: httpMethod(from: endpoint.method),
                                                parameters: endpoint.parameters)

            request.validate().responseData(queue: self.queue) { response in

                let result = response.result.flatMap(endpoint.decode)

                switch result {
                case let .success(val): observer(.success(val))
                case let .failure(err): observer(.error(err))
                }
            }

            return Disposables.create {

                request.cancel()
            }
        }
    }

    func downloadVideoWith(videoInfo: VideoInfo, title: String, atPath: URL, progressHandler:((Float)->Void)?) -> Single<Bool> {

        return Single<Bool>.create { observer in

            let destination: DownloadRequest.DownloadFileDestination = { _, _ in

                var path = atPath

                path.appendPathComponent(title.fixedTitle() + ".mp3")

                return (path, [.removePreviousFile])
            }

            let url = URL(string: "https:" + videoInfo.dloadUrl)

            let request = Alamofire.download(
                url!,
                method: .get,
                parameters: nil,
                encoding: JSONEncoding.default,
                headers: nil,
                to: destination).downloadProgress(closure: { (progress) in

                    progressHandler?(Float(progress.fractionCompleted))

                }).response(completionHandler: { (response) in

                    observer(.success(true))
                })

            return Disposables.create {

                request.cancel()
            }
        }

    }

    private func url(path: Path) -> URL {

        return URL(string: path)!
    }
}

public func httpMethod(from method: Method) -> Alamofire.HTTPMethod {

    switch method {
    case .get:    return .get
    case .post:   return .post
    case .put:    return .put
    case .patch:  return .patch
    case .delete: return .delete
    }
}


public class OAuth2Retrier: Alamofire.RequestRetrier {

    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {

        if (error as? AFError)?.responseCode == 401 {

            // TODO: implement your Auth2 refresh flow
            // See https://github.com/Alamofire/Alamofire#adapting-and-retrying-requests
        }

        completion(false, 0)
    }
}
