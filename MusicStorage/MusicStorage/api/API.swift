//
//  API.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation

enum API {}

extension API {

    enum Baixary {

        static func getVideoInfoForVideoWith(id: String) -> Endpoint<VideoData> {

            let path = "https://baixaryoutube.net/@api/json/mp3/" + id

            return Endpoint(method: .get, path: path, parameters: nil)
        }
    }
}
