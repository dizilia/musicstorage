//
//  VideoData.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation

class VideoData: Decodable {

    let vidID   : String
    let vidTitle: String
    let vidInfo : [String:VideoInfo]
}

class VideoInfo: Decodable {

    let dloadUrl: String
    let bitrate : Int
    let mp3size : String
}
