//
//  ListVC.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/20/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class ListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table               : UITableView!
    @IBOutlet weak var backToPreviousFolder: UIBarButtonItem!

    private var items :[DirectoryItem] = []
    private var fManager :MusicFileManager!

    override func viewDidLoad() {

        super.viewDidLoad()

        fManager = MusicFileManager()

        backToPreviousFolder.isEnabled = false
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        updateItems()
    }

    private func updateItems() {

        items = fManager.getItemsForCurrentFolder()

        table.reloadData()
    }

    @IBAction func addFolder(_ sender: Any) {

        getName {[unowned self] (name) in

            guard let folderName = name else {
                return
            }

            self.fManager.createDirectoryWith(name: folderName)

            self.updateItems()
        }
    }

    @IBAction func backToPrevFolder(_ sender: Any) {

        fManager.backToPreviousFolder()

        updateItems()
    }

    private func getName(didFinishWithName:((String?)->Void)?) {

        let alertController = UIAlertController(title: "Folder name", message: "", preferredStyle: UIAlertControllerStyle.alert)

        alertController.addTextField { (textField : UITextField!) -> Void in

            textField.placeholder = "Enter Folder Name"
        }

        let saveAction = UIAlertAction(title: "Apply", style: .default, handler: { alert -> Void in

            let textField = alertController.textFields![0] as UITextField

            didFinishWithName?(textField.text)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {

            (action : UIAlertAction!) -> Void in

            didFinishWithName?(nil)
        })


        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }

    //MARK:-
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath)

        cell.textLabel?.text = items[indexPath.row].path.pathComponents.last ?? "Unknown name"
        cell.textLabel?.textColor = items[indexPath.row].isFolder ? .blue : .black
        cell.textLabel?.font = items[indexPath.row].isFolder
                               ? UIFont.init(name: "HelveticaNeue-Bold", size: 15)
                               : UIFont.init(name: "HelveticaNeue-Italic", size: 14)

        cell.imageView?.image = items[indexPath.row].isFolder ? #imageLiteral(resourceName: "folder") : nil

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        fManager.delete(item: items[indexPath.row])
        updateItems()
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {

        return .delete
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let item = items[indexPath.row]

        if item.isFolder {

            fManager.changeDirectoryTo(name: item.path.pathComponents.last!)

            backToPreviousFolder.isEnabled = true

            updateItems()

        }else {

            let playlistItems = items.flatMap({ return $0.isFolder ? nil : PlaylistItem(url: $0.path) })

            UIRouter.instance.playerUIRouter.showPlayerFrom(parentVC: self, musicItems: playlistItems, currentItem: item)
        }
    }
}
