//
//  ListUIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/20/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class ListUIRouter {

    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!

    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "List", bundle: nil)
    }
}
