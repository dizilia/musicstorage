//
//  UIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation
import UIKit

class UIRouter {

    private var window    :UIWindow!
    public var currentVC  :UIViewController?
    private var session   :Session

    public static var instance: UIRouter!

    init(appDelegat :AppDelegate, session :Session) {

        self.session = session

        appDelegat.window = UIWindow(frame :UIScreen.main.bounds)

        window = appDelegat.window

        UIRouter.instance = self
    }

    public func showStartUpScreen() {

        window.rootViewController = homeUIRouter.homeVC()

        window.makeKeyAndVisible()

        currentVC = window.rootViewController
    }

    public lazy var homeUIRouter: HomeUIRouter = {

        return HomeUIRouter(mainRouter: self, session: session)
    }()

    public lazy var viewerUIRouter: ViewerUIRouter = {

        return ViewerUIRouter(mainRouter: self, session: session)
    }()

    public lazy var playerUIRouter: PlayerUIRouter = {

        return PlayerUIRouter(mainRouter: self, session: session)
    }()

    public lazy var сomponentsUIRouter: ComponentsUIRouter = {

        return ComponentsUIRouter(mainRouter: self, session: session)
    }()

}
