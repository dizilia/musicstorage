//
//  ViewerUIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class ViewerUIRouter {

    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!

    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "Viewer", bundle: nil)
    }

//    public func mapVC() -> MapVC {
//
//        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: MapVC.self)) as! MapVC
//
//        vc.session = session
//        vc.router  = mainRouter
//
//        return vc
//    }
}
