//
//  ViewerVC.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import MBProgressHUD

class ViewerVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!

    public var session: Session!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {

        super.viewDidLoad()

        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = false
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []

        //webView = WKWebView(frame: view.bounds, configuration: webConfiguration)

        webView.navigationDelegate = self

        let url = URL(string: "https://www.youtube.com")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }

    @IBAction func save(_ sender: Any) {

        router.сomponentsUIRouter.showFileManagerFrom(parentVC: self) {[weak self] (folderUrl) in

            self?.saveToFolderAt(path: folderUrl)
        }
    }

    private func saveToFolderAt(path: URL) {

        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .annularDeterminate
        hud.label.text = "Loading"

        session.getVideoInfoForVideo(id: webView.url!.absoluteString.videoId())
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] videoData in

                guard let self_ = self else {

                    hud.hide(animated: true)

                    return
                }

                guard let videoInfo = self_.session.getBestQualityVideoInfoFrom(videoData: videoData) else {
                    return
                }

                self_.session.downloadVideoWith(videoInfo: videoInfo, title: videoData.vidTitle, atPath: path, progressHandler: { fractions in

                    hud.progress = fractions
                })
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { result in

                        hud.hide(animated: true)

                    }, onError: nil).disposed(by: self_.disposeBag)

                }, onError: { error in

                    hud.hide(animated: true)

            }).disposed(by: disposeBag)

    }

    //MARK:- WKNavigation
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {

        print(navigation.description)
    }

    public func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {

        
    }
}
