//
//  UIView+MS.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/25/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    @IBInspectable var cornerRadius: CGFloat {

        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius  = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    //todo rename?
    func roundAllCornersWithRadius(_ radius: Float) {

        roundCorners(.allCorners, withRadius: radius)
    }

    //todo rename?
    func roundCorners(_ rectCorner :UIRectCorner, withRadius radius :Float) {

        if radius == 0 && layer.mask == nil {

            return
        }

        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: CGFloat(radius), height: CGFloat(radius)))

        let maskLayer = CAShapeLayer()

        maskLayer.frame = self.bounds
        maskLayer.path  = maskPath.cgPath

        layer.mask = maskLayer
    }
}
