//
//  String+MS.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation

extension String {

    public func videoId() -> String {

        var strings = self.components(separatedBy: "v=")

        strings = strings.last!.components(separatedBy: "&")

        return strings.first!
    }

    public func fixedTitle() -> String {

        var result = self.replacingOccurrences(of: "/", with: "_")

        return result
    }
}
