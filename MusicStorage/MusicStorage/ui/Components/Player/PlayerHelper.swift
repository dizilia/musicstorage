//
//  PlayerHelper.swift
//  MusicStorage
//
//  Created by Eugene Liashenko on 6/26/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation

class PlayerHelper {
    
    static func stringFromTime(interval: Int) -> String{
        let ti = NSInteger(interval)
        let seconds: NSInteger = ti % 60
        let minutes: NSInteger = (ti / 60) % 60
        let hours: NSInteger = (ti/3600)
        if hours > 0 {
            return NSString(format: "%02ld:%02ld:%02ld", hours, minutes, seconds) as String
        }
        return NSString(format: "%02ld:%02ld", minutes, seconds) as String
    }
}
