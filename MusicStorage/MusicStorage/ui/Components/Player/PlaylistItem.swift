//
//  PlaylistItem.swift
//  MusicStorage
//
//  Created by Eugene Liashenko on 6/26/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

private var playlistItemImageCache: [String: UIImage] = [:]

extension MPMediaItem {
    
    func asPlaylistItem() -> PlaylistItem {
        let item = PlaylistItem(url: self.assetURL!)
        
        item.title = self.title
        item.artist = self.artist
        item.albumName = self.albumTitle
        
        let audioAsset = AVURLAsset(url: URL(string: self.assetURL!.path)!, options: nil)
        let audioDuration = audioAsset.duration;
        let audioDurationSeconds = Int(CMTimeGetSeconds(audioDuration))
        
        item.durationOfFile = PlayerHelper.stringFromTime(interval: audioDurationSeconds)

        let cacheID = "\(item.artist!) | \(item.albumName!)"
        let kAlbumArtworkSize = CGSize(width: 256.0, height: 256.0)
        if let cached = playlistItemImageCache[cacheID] {
            item.artwork = cached
        } else if let artwork = self.artwork?.image(at: kAlbumArtworkSize) {
            playlistItemImageCache[cacheID] = artwork
            item.artwork = artwork
        }
        
        return item
    }
}

class PlaylistItem: AVPlayerItem {
    
    class func clearImageCache() {
        playlistItemImageCache.removeAll(keepingCapacity: false)
    }

    var durationOfFile: String = ""
    var url :URL!

    init(url: URL) {

        super.init(asset: AVAsset(url: url), automaticallyLoadedAssetKeys: nil)

        self.url = url
    }

    
    lazy var title: String? = {
        if let titleMetadataItem = AVMetadataItem.metadataItems(from: self.asset.commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: AVMetadataKeySpace.common).first {
            return titleMetadataItem.value as? String
        }
        return "untitled"
    }()
    
    lazy var artist: String? = {
        if let artistMetadataItem = AVMetadataItem.metadataItems(from: self.asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtist, keySpace: AVMetadataKeySpace.common).first {
            return artistMetadataItem.value as? String
        }
        return nil
    }()
    
    lazy var albumName: String? = {
        if let albumNameMetadataItem = AVMetadataItem.metadataItems(from: self.asset.commonMetadata, withKey: AVMetadataKey.commonKeyAlbumName, keySpace: AVMetadataKeySpace.common).first {
            return albumNameMetadataItem.value as? String
        }
        return nil
    }()
    
    lazy var artwork: UIImage? = {
        
        var cacheID: String = ""
        if self.artist != nil && self.albumName != nil {
            cacheID = "\(self.artist) | \(self.albumName)"
        }
        
        if let cached = playlistItemImageCache[cacheID] {
            return cached
        } else if let artworkMetadataItem = AVMetadataItem.metadataItems(from: self.asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtwork, keySpace: AVMetadataKeySpace.common).first {
            
            if let artworkMetadataDictionary = artworkMetadataItem.value as? [String: AnyObject] {
                if let artworkData = artworkMetadataDictionary["data"] as? NSData {
                    if let image = UIImage(data: artworkData as Data) {
                        playlistItemImageCache[cacheID] = image
                        return image
                    }
                }
            } else if let artworkData = artworkMetadataItem.value as? NSData {
                if let image = UIImage(data: artworkData as Data) {
                    playlistItemImageCache[cacheID] = image
                    return image
                }
            }
            
        }
        return nil
    }()
}
