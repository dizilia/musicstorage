//
//  FIleManagerVC.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/25/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class FIleManagerVC: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var backBtn: UIBarButtonItem!

    public var session:         Session!
    public var didSelectFolder: ((URL)->Void)?

    fileprivate var items :[DirectoryItem] = []

    private var fManager :MusicFileManager!

    override func viewDidLoad() {

        super.viewDidLoad()

        fManager = MusicFileManager()

        if let currentFolder = session.currentFolder {

            fManager.currentFolder = currentFolder
            backBtn.isEnabled = true

        }else {

            backBtn.isEnabled = false
        }

        table.tableFooterView = UIView()

        updateItems()
    }

    private func updateItems() {

        items = fManager.getItemsForCurrentFolder()

        table.reloadData()
    }

    @IBAction func selectFolder(_ sender: Any) {

        session.currentFolder = fManager.currentFolder

        didSelectFolder?(fManager.currentFolder)

        close(self)
    }

    @IBAction func close(_ sender: Any) {

        router.сomponentsUIRouter.backFromFIleManager(vc: self)
    }
    
    @IBAction func back(_ sender: Any) {

        fManager.backToPreviousFolder()

        updateItems()
    }

    @IBAction func addFolder(_ sender: Any) {

        getName {[unowned self] (name) in

            guard let folderName = name else {
                return
            }

            self.fManager.createDirectoryWith(name: folderName)

            self.updateItems()
        }
    }

    private func getName(didFinishWithName:((String?)->Void)?) {

        let alertController = UIAlertController(title: "Folder name", message: "", preferredStyle: UIAlertControllerStyle.alert)

        alertController.addTextField { (textField : UITextField!) -> Void in

            textField.placeholder = "Enter Folder Name"
        }

        let saveAction = UIAlertAction(title: "Apply", style: .default, handler: { alert -> Void in

            let textField = alertController.textFields![0] as UITextField

            didFinishWithName?(textField.text)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {

            (action : UIAlertAction!) -> Void in

            didFinishWithName?(nil)
        })


        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    //
}

extension FIleManagerVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "FileManagerCell", for: indexPath)

        cell.textLabel?.text      = items[indexPath.row].path.pathComponents.last ?? "Unknown name"
        cell.textLabel?.textColor = items[indexPath.row].isFolder ? .blue : .black

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        //didSelectFolder?(items[indexPath.row].path)

        let item = items[indexPath.row]

        if item.isFolder {

            fManager.changeDirectoryTo(name: item.path.pathComponents.last!)

            backBtn.isEnabled = true

            updateItems()

        }else {

            //UIRouter.instance.playerUIRouter.showPlayerFrom(parentVC: self, musicUrl: item.path)
        }
    }
}
