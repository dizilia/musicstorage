//
//  ComponentsUIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/25/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class ComponentsUIRouter {

    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!

    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "Components", bundle: nil)
    }

    public func showFileManagerFrom(parentVC: UIViewController, didSelectFolder: ((URL)->Void)?) {

        let vc = storyboard.instantiateViewController(withIdentifier: String(describing:FIleManagerVC.self)) as! FIleManagerVC

        vc.router          = mainRouter
        vc.session         = session
        vc.didSelectFolder = didSelectFolder

        mainRouter.currentVC = vc

        parentVC.present(vc, animated: true, completion: nil)
    }

    public func backFromFIleManager(vc: FIleManagerVC) {

        mainRouter.currentVC = vc.presentingViewController

        vc.dismiss(animated: true, completion: nil)
    }
}
