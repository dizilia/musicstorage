//
//  PlayerVC.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/20/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit
import AVFoundation

let kTestImage = "Icon"
let kTestTimeInterval = 20.0

class PlayerVC: UIViewController {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var fastforwardButton: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!

    public var musicItems = [PlaylistItem]()
    public var currentItem :DirectoryItem!

    private var player: MusicPlayer!
    var sliderTimer: Timer?

    override func viewDidLoad() {

        super.viewDidLoad()

        player = MusicPlayer()
        player.delegate = self

        MusicPlayer.initSession()

        player.playlist = musicItems

        let currentItemIndex: Int = musicItems.index(where: { $0.url == currentItem.path })!
        player.setCurrentItemFromIndex(index: currentItemIndex)
//        player.currentItem = musicItems.first(where: { playlistItem in
//
//            return playlistItem.url == currentItem.path
//        })

        setPlayButton(paused: true)

        hideLoadingIndicator()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( animated )

        NotificationCenter.default.addObserver(self, selector: #selector(PlayerVC.episodeLoadedNotification(_:)), name: .mediaLoadProgress, object: nil)

        playButtonPressed(self)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.removeObserver( self )

        player.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Notifications

    @objc func episodeLoadedNotification(_ notification: Notification) {
        if let percentage: NSNumber = notification.object as? NSNumber {
            self.statusLabel.text = "\(percentage.intValue)% Loaded"
        }
    }

    func hideLoadingIndicator() {
        self.playButton.isHidden = false
        self.loadingIndicator.isHidden = true

        self.loadingIndicator.stopAnimating()
    }

    //MARK:-
    @IBAction func playButtonPressed(_ sender: AnyObject) {

        if player.paused() {
            player.play()

            setPlayButton(paused: false)

        }else {
            player.pause()

            setPlayButton(paused: true)
        }
    }

    @IBAction func rewindButtonPressed(_ sender: AnyObject) {

        player.previousTrack()
    }

    @IBAction func fastforwardButtonPressed(_ sender: AnyObject) {

        player.nextTrack()
    }

    @IBAction func sliderValueChanged(_ sender: UISlider) {

      //  let duration = Float(CMTimeGetSeconds(player.currentItem!.asset.duration)) * sender.value

   //     player.pause()

    //    print(CMTimeGetSeconds(player.currentItem!.asset.duration))
    //    print(sender.value)

//
        player.currentItem?.seek(to: CMTimeMakeWithSeconds(Float64(sender.value), 1), completionHandler: nil)

   //     player.play()
    }

    @IBAction func shuffle(_ sender: Any) {

        player.shuffle()
    }

    private func setPlayButton(paused: Bool) {

        let playPauseImage = paused ? UIImage(named: "play") : UIImage(named: "pause")

        self.playButton.setImage(playPauseImage, for: UIControlState())
    }

    func setupSlider() {

        let duration = CMTimeGetSeconds(player.currentItem!.asset.duration)

        self.progressSlider.maximumValue = Float(duration)
        self.progressSlider.minimumValue = 0.0

        if let _ = self.sliderTimer {
            self.sliderTimer?.invalidate()
        }

        self.sliderTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PlayerVC.sliderTimerTriggered), userInfo: nil, repeats: true)

        self.setupTotalTimeLabel()
    }

    @objc func sliderTimerTriggered() {

        let playerCurrentTime = CMTimeGetSeconds(player.currentItem!.currentTime())

        self.progressSlider.value = Float( playerCurrentTime )

        self.updateCurrentTimeLabel(Float( playerCurrentTime))
    }

    func updateCurrentTimeLabel(_ currentTimeInSeconds: Float) {
        if currentTimeInSeconds.isNaN || currentTimeInSeconds.isInfinite {
            return
        }

        currentTimeLabel.text = timeLabelString( Int( currentTimeInSeconds ) )
    }

    func setupTotalTimeLabel() {

        let duration = CMTimeGetSeconds(player.currentItem!.asset.duration)

        if duration.isNaN || duration.isInfinite {
            return
        }

        totalTimeLabel.text = timeLabelString( Int (duration) )
    }

    func showLoadingIndicator() {
        self.playButton.isHidden = true
        self.loadingIndicator.isHidden = false

        self.loadingIndicator.startAnimating()
    }

    func timeLabelString(_ duration: Int) -> String {
        let currentMinutes = Int(duration) / 60
        let currentSeconds = Int(duration) % 60

        return currentSeconds < 10 ? "\(currentMinutes):0\(currentSeconds)" : "\(currentMinutes):\(currentSeconds)"
    }

}

extension PlayerVC: MusicPlayerDelegate {

    func player(playlistPlayer: MusicPlayer, didChangeCurrentPlaylistItem playlistItem: PlaylistItem?) {

        self.sliderTimer?.invalidate()

        titleLabel.text = playlistItem?.url.lastPathComponent
        totalTimeLabel.text = playlistItem?.durationOfFile

        setupSlider()
    }
}
