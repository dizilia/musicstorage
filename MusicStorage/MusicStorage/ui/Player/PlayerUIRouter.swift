//
//  PlayerUIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/20/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class PlayerUIRouter {

    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!

    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "Player", bundle: nil)
    }

    public func showPlayerFrom(parentVC: UIViewController, musicItems: [PlaylistItem], currentItem: DirectoryItem) {

        let vc = storyboard.instantiateViewController(withIdentifier: String(describing:PlayerVC.self)) as! PlayerVC

        vc.musicItems  = musicItems
        vc.currentItem = currentItem

        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
}
