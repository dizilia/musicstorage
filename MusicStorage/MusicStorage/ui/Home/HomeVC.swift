//
//  HomeVC.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class HomeVC: UITabBarController {

    public var session: Session!

    override func viewDidLoad() {

        super.viewDidLoad()

        self.viewControllers?.forEach({[unowned self] vc in

            if let viewerVC = vc as? ViewerVC {

                viewerVC.session = session
                viewerVC.router  = router
            }
        })
    }
}
