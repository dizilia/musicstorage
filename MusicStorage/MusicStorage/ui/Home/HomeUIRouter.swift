//
//  HomeUIRouter.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import UIKit

class HomeUIRouter {

    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!

    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "Home", bundle: nil)
    }

    public func homeVC() -> HomeVC {

        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC

        vc.session = session
        vc.router  = mainRouter

        return vc
    }
}

