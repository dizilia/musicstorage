//
//  MusicFileManager.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/22/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation
import UIKit

public struct DirectoryItem {

    public var path: URL!
    public var isFolder: Bool = false
}

class MusicFileManager {

    public var rootDirectory :URL!
    public var currentFolder :URL!

    init() {

        let fileManager = FileManager.default

        rootDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

        currentFolder = rootDirectory
    }

    public func getItemsForCurrentFolder() -> [DirectoryItem] {

        var items = [DirectoryItem]()

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: currentFolder, includingPropertiesForKeys: nil)

            fileURLs.forEach({ (url) in

                items.append(DirectoryItem(path: url, isFolder: url.absoluteString.last! == "/"))
            })

            return sorted(items: items)

        } catch {

            print("Error while enumerating files \(rootDirectory.path): \(error.localizedDescription)")

            return sorted(items: items)
        }
    }

    public func createDirectoryWith(name: String) {

        let newDir = currentFolder.appendingPathComponent(name).path

        do {
            try FileManager.default.createDirectory(atPath: newDir,
                               withIntermediateDirectories: true,
                                                attributes: nil)

        } catch let error as NSError {

            print("Error: \(error.localizedDescription)")
        }
    }

    public func changeDirectoryTo(name: String) {

        currentFolder = currentFolder.appendingPathComponent(name)
    }

    public func backToPreviousFolder() {

        if currentFolder == rootDirectory {
            return
        }

        currentFolder = currentFolder.deletingLastPathComponent()
    }

    public func delete(item: DirectoryItem) {

        try? FileManager.default.removeItem(at: item.path)
    }

    private func sorted(items:[DirectoryItem]) -> [DirectoryItem] {

        return items.sorted(by: { (item1, item2)  in

            if item1.isFolder && item2.isFolder {
                return true
            }

            if item1.isFolder && !item2.isFolder {
                return true
            }

            if !item1.isFolder && item2.isFolder {
                return false
            }

            if !item1.isFolder && !item2.isFolder {
                return true
            }

            return false
        })
    }
}
