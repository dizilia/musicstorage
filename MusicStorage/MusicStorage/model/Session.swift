//
//  Session.swift
//  MusicStorage
//
//  Created by TS.MAC on 6/18/18.
//  Copyright © 2018 PepperAndSalt. All rights reserved.
//

import Foundation
import RxSwift

class Session {

    private let apiClient: APIClient

    public var currentFolder :URL?

    init() {

        apiClient = APIClient()
    }

    public func getVideoInfoForVideo(id: String) -> Single<VideoData> {

        return apiClient.request(API.Baixary.getVideoInfoForVideoWith(id: id))
    }

    public func getBestQualityVideoInfoFrom(videoData: VideoData) -> VideoInfo? {

        let bestVideoInfo = videoData.vidInfo.first(where: { (key, info) -> Bool in

            return info.bitrate > 256
        })?.value

        return bestVideoInfo
    }

    public func downloadVideoWith(videoInfo: VideoInfo, title: String, atPath path: URL, progressHandler:((Float)->Void)?) -> Single<Bool> {

        return apiClient.downloadVideoWith(videoInfo: videoInfo, title: title, atPath: path, progressHandler: progressHandler)
    }
}
